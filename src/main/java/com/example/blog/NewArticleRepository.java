package com.example.blog;


import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface NewArticleRepository extends MongoRepository<NewArticle, String>{

        public NewArticle findByTitle(String title);
        public List<NewArticle> findByText(String text);

}
