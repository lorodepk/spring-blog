package com.example.blog;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CommentsRepository extends MongoRepository<Comment, String>{
    public List<Comment> findByApplicationId(String applicationId);
    public List<Comment> deleteByApplicationId(String applicationId);
}
