package com.example.blog;

import org.springframework.data.annotation.Id;


public class NewArticle {

        @Id
        private String id;

        public void setId(String id) {
            this.id = id;
        }

    private String title;
        private String text;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getId(){
            return id;
        }


        @Override
        public String toString() {
            return String.format(
                    "newArticle[id = '%s', title='%s', text='%s']",
                    id, title, text);
        }


}
