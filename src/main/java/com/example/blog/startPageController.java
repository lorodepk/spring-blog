package com.example.blog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



@Controller
public class startPageController{
    @Autowired
    private NewArticleRepository newArticleRepository;
    @Autowired
    private CommentsRepository commentsRepository;
    private String selectIdApp;
    private boolean showUpdateInput = false;

    @RequestMapping(value="/")
    public String startPage(NewArticle newArticle, Model model){
        if(newArticle.getTitle()!= null && newArticle.getText() != null && !newArticle.getTitle().equals("") && !newArticle.getText().equals("")){
            newArticleRepository.save(newArticle);
        }
        model.addAttribute("newArticle", newArticleRepository.findAll());
        return "startPage";
    }


    @RequestMapping(value="/addNewArticle", method = RequestMethod.GET)
    public String openPageNewArticle(Model model){
        model.addAttribute("newArticle", new NewArticle());
        return "addNewArticle";
    }

    @RequestMapping(value="/addNewArticle", method = RequestMethod.POST)
    public String addNewArticle(NewArticle newArticle, String title, String text){
        newArticle.setTitle(title);
        newArticle.setText(text);
        return "startPage";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String openArticle(@PathVariable(value = "id") NewArticle newArticle, Model model, Comment comment){
        selectIdApp = newArticle.getId();
        model.addAttribute("comments",commentsRepository.findByApplicationId(newArticle.getId()));
        model.addAttribute("newArticle", newArticle);
        model.addAttribute("addComment", new Comment());
        model.addAttribute("showUpdateInput",showUpdateInput);
        return "article";
    }

    @RequestMapping(value = "/show_update_input")
    public String showUpdateInput(){
        showUpdateInput = !showUpdateInput;
        return "redirect:" +selectIdApp;
    }

    @RequestMapping(value = "/delete_app")
    public String delete_app(){
        commentsRepository.deleteByApplicationId(selectIdApp);
        newArticleRepository.deleteById(selectIdApp);
        selectIdApp = null;
        showUpdateInput = !showUpdateInput;
        return "redirect:/";
    }

    @RequestMapping(value = "/update_ap", method =RequestMethod.POST)
    public String updateApplication(String title, String text, NewArticle newArticle){
        if(title != null && !title.equals("") && text != null && !text.equals("") && selectIdApp != null){
            newArticle.setTitle(title);
            newArticle.setText(text);
            newArticle.setId(selectIdApp);
            newArticleRepository.save(newArticle);
        }
        showUpdateInput = !showUpdateInput;
        return "redirect:" +selectIdApp;
    }

    @RequestMapping(value = "/add_comment", method = RequestMethod.POST)
    public String addComment(Comment comment, String userName, String userComment){
        comment.setUserName(userName);
        comment.setUserComment(userComment);
        comment.setApplicationId(selectIdApp);
        commentsRepository.save(comment);
        return "redirect:" +selectIdApp;
    }


}
