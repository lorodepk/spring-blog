package com.example.blog;

import org.springframework.data.annotation.Id;

public class Comment {

    @Id
    private String id;

    private String userName;
    private String userComment;
    private String applicationId;

    public String getId() {
        return id;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getUserComment() {
        return userComment;
    }

    public void setUserComment(String userComment) {
        this.userComment = userComment;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return String.format(
                "Comment[id = '%s', userName='%s', userComment='%s', applicationId='%s']",
                id, userName, userComment,applicationId);
    }
}
